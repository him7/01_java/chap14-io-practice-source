package com.ohgriaffers.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileController {
	protected Scanner sc = new  Scanner(System.in);
	
	public FileController() {}

	public void fileSave() {
		StringBuilder sb = new StringBuilder();
		String text;
		int cnt = 0;
		while(true) {
			System.out.println("파일에 저장할 내용을 입력하시오(\"exit\"을 입력하면 내용 입력 끝) : ");
			text = sc.nextLine();
			if(text.equals("exit")) {
				break;
			} else {
				if(cnt != 0) {
					sb.append(text != null ? text : "").append(System.getProperty("line.separator"));
				} else {
					sb.append(text);
				}
			}
		}
		sc.nextLine();
		System.out.print("저장하시겠습니까? (y/n) : ");
		char isYn = Character.toUpperCase(sc.next().charAt(0));
		String tit;
		if(isYn == 'Y') {
			sc.nextLine();
			System.out.print("파일 명을 입력해주세요 : ");
			tit = sc.nextLine();
			tit = tit.concat(".txt");
			
			FileWriter fw = null;
			BufferedWriter bw = null;
			
			String path = "upload";
			File file = new File(path);
			if(!file.exists()) {
				file.mkdir();
			}
			
			try {
				fw = new FileWriter("upload/"+tit);
				bw = new BufferedWriter(fw);
				System.out.println(sb.toString());
				bw.write(sb.toString());
				bw.flush();
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bw != null) {
					try {
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(fw != null) {
					try {
						fw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		}else {
			System.out.println("다시 메뉴로 돌아갑니다.\n");
			return;
		}
		
	}
	
	public void fileOpen() {
		System.out.print("열기 할 파일명 : ");
		String tit = sc.nextLine();
		BufferedReader br = null;
		FileReader fr = null;
		String path = "upload/"+tit+".txt";
		
		try {
			File file = new File(path);
			
			if(file.exists()) {
				fr = new FileReader(path);
				br = new BufferedReader(fr);
				
				String txt;
				while((txt = br.readLine()) != null) {
					System.out.println(txt);
				}
				System.out.println();
			} else {
				System.out.println("노트가 없습니다.\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fr != null) {
				try {
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void fileEdit() {
		StringBuilder sb = new StringBuilder();
		
		System.out.print("수정할 파일 명 : ");
		String tit = sc.nextLine();
		String path = "upload/"+tit+".txt";
		sc.nextLine();
		File file = new File(path);
		BufferedWriter bw = null;
		BufferedReader br = null;
		if(file.exists()) {
			try {
				bw = new BufferedWriter(new FileWriter(path, true));
				br = new BufferedReader(new FileReader(path));
				
				String txt;
				while((txt = br.readLine()) != null) {
					System.out.println(txt);
				}
				System.out.println();
				
				while(true) {
					System.out.print("파일에 추가할 내용을 입력하세요 (나가기 exit 입력): ");
					String alter = sc.nextLine();

					if(alter.equals("exit")) {
						break;
					} else {
						sb.append(alter != null ? alter : "").append(System.getProperty("line.separator"));
					}
				}
				
				// “변경된 내용을 파일에 추가하시겠습니까? (y/n)”
				// 입력 받은 값이 대문자이든 소문자이든 상관없이 “y”이면,
				// “입력받은 파일명.txt 파일의의 내용이 변경되었습니다.”
				
				System.out.print("변경된 내용을 파일에 추가하시겠습니까? (y/n) : ");
				char isYn = Character.toUpperCase(sc.next().charAt(0));
				if(isYn == 'Y') {
					String temp = sb.toString();
					bw.write(temp);
					bw.flush();
					System.out.println("입력받은 "+tit+".txt 파일의 내용이 변경되었습니다.\n");
				}
				
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(bw != null) {
					try {
						bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("노트가 존재하지 않습니다.\n");
			return;
		}
	}
	
	
	
}
